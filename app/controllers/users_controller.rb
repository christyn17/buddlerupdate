class UsersController < ApplicationController
  
  def index
   # @user = current_user
   # @posts = @user.posts
    @user = User.find(params[:search])
    @posts = @user.posts
  end

  def show
  	@user = User.find(params[:id])
  	@posts = @user.posts
  end

  def add_friend
    
  end
  
  def remove_friend
    
  end

end