module Api
  module V1

  class RegistrationsController < Devise::RegistrationsController
  protect_from_forgery
  respond_to :json

  def create
      user = User.new(params[:user])
      if user.save
          render :json=> user.as_json(:email=>user.email), :status=>201
        return
      else
        warden.custom_failure!
        render :json=> user.errors, :status=>422
      end
  end

  private

    def user_params
      params.require(:user).permit(:firstname, :lastname, :email)
    end

end

  
  end
end