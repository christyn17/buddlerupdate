module Api
  module V1

    class UsersController < ApplicationController
    protect_from_forgery
    respond_to :json 
   
    def index
      # @users = User.all
      respond_with User.all
      # respond_to do |format|
      #   format.json { render json: @users }
      #   format.xml { render xml: @users }
      
    end

    def show
  	  respond_with User.find(params[:id])
    end

    def update
      @user = User.find(params[:id])
      if @user.update(user_params)
         render :json=> @user.as_json(:email=>@user.email), :status=>200
      else
          warden.custom_failure!
          render :json=> user.errors, :status=>204
      end
    end

    def edit
    
    end
    
    private

    def user_params
      params.require(:user).permit(:firstname, :lastname, :email)
    end

    def find_user
      @user = User.find(params[:id])     
    end
    end

  end
end