module Api
  module V1

	class PostsController < ApplicationController

before_action :find_post, only: [:show, :edit, :update, :destroy]
	before_action :authenticate_user!, except: [:index, :show] 
	protect_from_forgery
    respond_to :json 
   
	def index
		respond_with Post.all
		# @user = current_user
		# @posts = Post.all.order("created_at DESC")
		# @post = current_user.posts.build
		# ids = current_user.friends.pluck(:id) << current_user.id
		# @posts = Post.where(user_id: ids)
	end

	def show
		
	end

	def new
		@post = current_user.posts.build
	end

	def create
		@post = current_user.posts.build(post_params)
		if @post.save
			redirect_to root_path
		else
			render 'new'
		end
	end

	def update
      @post = Post.find(params[:id])
      if @post.update(post_params)
         render :json=> @post.as_json(:user_id=>@post.user_id), :status=>200
      else
          warden.custom_failure!
          render :json=> user.errors, :status=>204
      end
    end

    def edit
    
    end
    
	def destroy
   		@post.destroy
   		redirect_to root_path
  end

	private
		def post_params
			params.require(:post).permit(:status)
		end

		def find_post
			@post = Post.find(params[:id])
		end
	
	end
	
  end
end
