module Api
  module V1

    class FriendshipsController < ApplicationController

	  def create
 		 @friendship = current_user.friendships.build(:friend_id => params[:friend_id])
  	   if @friendship.save
   				 
 			 else
    			
  		 end
	  end

	  def destroy
 		 @friendship = current_user.friendships.find(params[:id])
  		@friendship.destroy
  		flash[:notice] = "Removed friendship."
  		redirect_to current_user
  	end

	  def show
		 @friendship = current_user.friendships.find(params[:id])
		 @user = @friendship.friend		
     @posts = @user.posts
    end

    end

  end
end
