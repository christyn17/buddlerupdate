module Api
  module V1

    class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.


      before_filter :configure_permitted_parameters, if: :devise_controller?
  
      protected
  

      def configure_permitted_parameters
    
      end

      protect_from_forgery with: :exception

      def update_resource(resource, params)
    
      end

      def edit
   
      end

      def index
     
      end

      def search
        if params[:search]
      
        else
     
        end
      end


      def destroy
    
      end
  	
    end
    
  end
end
