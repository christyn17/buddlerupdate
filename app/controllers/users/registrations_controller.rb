  class Users::RegistrationsController < Devise::RegistrationsController
  
  respond_to :json

  def create
      user = User.new(params[:user])

      if user.save
          render :json=> user.as_json(:email=>user.email), :status=>201
      else
        warden.custom_failure!
        render :json=> user.errors, :status=>422
      end
  end

 def update
    if @user.update(user_params)
      redirect_to current_user
    else
      render 'edit'
    end
  end

  def edit
    
  end

  private

    def user_params
      params.require(:user).permit(:firstname, :lastname, :email)
    end

  def find_user
    @user = User.find(params[:id])     
  end

end